// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Medic App`
  String get appName {
    return Intl.message(
      'Medic App',
      name: 'appName',
      desc: '',
      args: [],
    );
  }

  /// `Title Onboarding 1`
  String get titleOnboarding1 {
    return Intl.message(
      'Title Onboarding 1',
      name: 'titleOnboarding1',
      desc: '',
      args: [],
    );
  }

  /// `Desc Onboarding 1`
  String get descOnboarding1 {
    return Intl.message(
      'Desc Onboarding 1',
      name: 'descOnboarding1',
      desc: '',
      args: [],
    );
  }

  /// `Title Onboarding 2`
  String get titleOnboarding2 {
    return Intl.message(
      'Title Onboarding 2',
      name: 'titleOnboarding2',
      desc: '',
      args: [],
    );
  }

  /// `Title Onboarding 3`
  String get titleOnboarding3 {
    return Intl.message(
      'Title Onboarding 3',
      name: 'titleOnboarding3',
      desc: '',
      args: [],
    );
  }

  /// `Title Onboarding 4`
  String get titleOnboarding4 {
    return Intl.message(
      'Title Onboarding 4',
      name: 'titleOnboarding4',
      desc: '',
      args: [],
    );
  }

  /// `Desc Onboarding2`
  String get descOnboarding2 {
    return Intl.message(
      'Desc Onboarding2',
      name: 'descOnboarding2',
      desc: '',
      args: [],
    );
  }

  /// `Desc Onboarding3`
  String get descOnboarding3 {
    return Intl.message(
      'Desc Onboarding3',
      name: 'descOnboarding3',
      desc: '',
      args: [],
    );
  }

  /// `Desc Onboarding4`
  String get descOnboarding4 {
    return Intl.message(
      'Desc Onboarding4',
      name: 'descOnboarding4',
      desc: '',
      args: [],
    );
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get register {
    return Intl.message(
      'Register',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `Email`
  String get email {
    return Intl.message(
      'Email',
      name: 'email',
      desc: '',
      args: [],
    );
  }

  /// `Nama Depan`
  String get namaDepan {
    return Intl.message(
      'Nama Depan',
      name: 'namaDepan',
      desc: '',
      args: [],
    );
  }

  /// `Nama Belakang`
  String get namaBelakang {
    return Intl.message(
      'Nama Belakang',
      name: 'namaBelakang',
      desc: '',
      args: [],
    );
  }

  /// `No. KTP`
  String get noKtp {
    return Intl.message(
      'No. KTP',
      name: 'noKtp',
      desc: '',
      args: [],
    );
  }

  /// `No. Telpon`
  String get noTelpon {
    return Intl.message(
      'No. Telpon',
      name: 'noTelpon',
      desc: '',
      args: [],
    );
  }

  /// `Masukkan Nama Depan`
  String get masukkanNamaDepan {
    return Intl.message(
      'Masukkan Nama Depan',
      name: 'masukkanNamaDepan',
      desc: '',
      args: [],
    );
  }

  /// `Masukkan Nama Belakang`
  String get masukkanNamaBelakang {
    return Intl.message(
      'Masukkan Nama Belakang',
      name: 'masukkanNamaBelakang',
      desc: '',
      args: [],
    );
  }

  /// `Masukkan No. KTP anda`
  String get masukkanNoKtp {
    return Intl.message(
      'Masukkan No. KTP anda',
      name: 'masukkanNoKtp',
      desc: '',
      args: [],
    );
  }

  /// `Masukkan Email`
  String get masukkanEmail {
    return Intl.message(
      'Masukkan Email',
      name: 'masukkanEmail',
      desc: '',
      args: [],
    );
  }

  /// `Masukkan No. Telpon Anda`
  String get masukkanTelp {
    return Intl.message(
      'Masukkan No. Telpon Anda',
      name: 'masukkanTelp',
      desc: '',
      args: [],
    );
  }

  /// `Please fill valid email`
  String get pleaseFillValidEmail {
    return Intl.message(
      'Please fill valid email',
      name: 'pleaseFillValidEmail',
      desc: '',
      args: [],
    );
  }

  /// `Masukkan password...`
  String get masukkanKataSandi {
    return Intl.message(
      'Masukkan password...',
      name: 'masukkanKataSandi',
      desc: '',
      args: [],
    );
  }

  /// `Masukkan password anda...`
  String get masukkanKataSandiAnda {
    return Intl.message(
      'Masukkan password anda...',
      name: 'masukkanKataSandiAnda',
      desc: '',
      args: [],
    );
  }

  /// `Masukkan konfirmasi password...`
  String get masukkanKonfirmasiKataSandi {
    return Intl.message(
      'Masukkan konfirmasi password...',
      name: 'masukkanKonfirmasiKataSandi',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get kataSandi {
    return Intl.message(
      'Password',
      name: 'kataSandi',
      desc: '',
      args: [],
    );
  }

  /// `Konfirmasi Password`
  String get konfirmasiKataSandi {
    return Intl.message(
      'Konfirmasi Password',
      name: 'konfirmasiKataSandi',
      desc: '',
      args: [],
    );
  }

  /// `Password tidak boleh kosong`
  String get thePasswordMustNotBeEmpty {
    return Intl.message(
      'Password tidak boleh kosong',
      name: 'thePasswordMustNotBeEmpty',
      desc: '',
      args: [],
    );
  }

  /// `Lupa password?`
  String get lupaKataSandi {
    return Intl.message(
      'Lupa password?',
      name: 'lupaKataSandi',
      desc: '',
      args: [],
    );
  }

  /// `Kurang dari 8 karakter`
  String get passwordKurangKarakter {
    return Intl.message(
      'Kurang dari 8 karakter',
      name: 'passwordKurangKarakter',
      desc: '',
      args: [],
    );
  }

  /// `Wajib diisi`
  String get required {
    return Intl.message(
      'Wajib diisi',
      name: 'required',
      desc: '',
      args: [],
    );
  }

  /// `Ya`
  String get yes {
    return Intl.message(
      'Ya',
      name: 'yes',
      desc: '',
      args: [],
    );
  }

  /// `Tidak`
  String get no {
    return Intl.message(
      'Tidak',
      name: 'no',
      desc: '',
      args: [],
    );
  }

  /// `Nama`
  String get name {
    return Intl.message(
      'Nama',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `Deskripsi`
  String get description {
    return Intl.message(
      'Deskripsi',
      name: 'description',
      desc: '',
      args: [],
    );
  }

  /// `Konfirmasi`
  String get confirmation {
    return Intl.message(
      'Konfirmasi',
      name: 'confirmation',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get keluar {
    return Intl.message(
      'Logout',
      name: 'keluar',
      desc: '',
      args: [],
    );
  }

  /// `Kirim`
  String get submit {
    return Intl.message(
      'Kirim',
      name: 'submit',
      desc: '',
      args: [],
    );
  }

  /// `Apakah Anda yakin ingin logout?`
  String get msgLogoutConfirmation {
    return Intl.message(
      'Apakah Anda yakin ingin logout?',
      name: 'msgLogoutConfirmation',
      desc: '',
      args: [],
    );
  }

  /// `Gagal Logout`
  String get msgFailedToLogout {
    return Intl.message(
      'Gagal Logout',
      name: 'msgFailedToLogout',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'id'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
