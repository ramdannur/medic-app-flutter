// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "appName": MessageLookupByLibrary.simpleMessage("Medic App"),
        "confirmation": MessageLookupByLibrary.simpleMessage("Konfirmasi"),
        "descOnboarding1":
            MessageLookupByLibrary.simpleMessage("Desc Onboarding 1"),
        "descOnboarding2":
            MessageLookupByLibrary.simpleMessage("Desc Onboarding2"),
        "descOnboarding3":
            MessageLookupByLibrary.simpleMessage("Desc Onboarding3"),
        "descOnboarding4":
            MessageLookupByLibrary.simpleMessage("Desc Onboarding4"),
        "description": MessageLookupByLibrary.simpleMessage("Deskripsi"),
        "email": MessageLookupByLibrary.simpleMessage("Email"),
        "kataSandi": MessageLookupByLibrary.simpleMessage("Password"),
        "keluar": MessageLookupByLibrary.simpleMessage("Logout"),
        "konfirmasiKataSandi":
            MessageLookupByLibrary.simpleMessage("Konfirmasi Password"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "lupaKataSandi": MessageLookupByLibrary.simpleMessage("Lupa password?"),
        "masukkanEmail": MessageLookupByLibrary.simpleMessage("Masukkan Email"),
        "masukkanKataSandi":
            MessageLookupByLibrary.simpleMessage("Masukkan password..."),
        "masukkanKataSandiAnda":
            MessageLookupByLibrary.simpleMessage("Masukkan password anda..."),
        "masukkanKonfirmasiKataSandi": MessageLookupByLibrary.simpleMessage(
            "Masukkan konfirmasi password..."),
        "masukkanNamaBelakang":
            MessageLookupByLibrary.simpleMessage("Masukkan Nama Belakang"),
        "masukkanNamaDepan":
            MessageLookupByLibrary.simpleMessage("Masukkan Nama Depan"),
        "masukkanNoKtp":
            MessageLookupByLibrary.simpleMessage("Masukkan No. KTP anda"),
        "masukkanTelp":
            MessageLookupByLibrary.simpleMessage("Masukkan No. Telpon Anda"),
        "msgFailedToLogout":
            MessageLookupByLibrary.simpleMessage("Gagal Logout"),
        "msgLogoutConfirmation": MessageLookupByLibrary.simpleMessage(
            "Apakah Anda yakin ingin logout?"),
        "namaBelakang": MessageLookupByLibrary.simpleMessage("Nama Belakang"),
        "namaDepan": MessageLookupByLibrary.simpleMessage("Nama Depan"),
        "name": MessageLookupByLibrary.simpleMessage("Nama"),
        "no": MessageLookupByLibrary.simpleMessage("Tidak"),
        "noKtp": MessageLookupByLibrary.simpleMessage("No. KTP"),
        "noTelpon": MessageLookupByLibrary.simpleMessage("No. Telpon"),
        "passwordKurangKarakter":
            MessageLookupByLibrary.simpleMessage("Kurang dari 8 karakter"),
        "pleaseFillValidEmail":
            MessageLookupByLibrary.simpleMessage("Please fill valid email"),
        "register": MessageLookupByLibrary.simpleMessage("Register"),
        "required": MessageLookupByLibrary.simpleMessage("Wajib diisi"),
        "submit": MessageLookupByLibrary.simpleMessage("Kirim"),
        "thePasswordMustNotBeEmpty":
            MessageLookupByLibrary.simpleMessage("Password tidak boleh kosong"),
        "titleOnboarding1":
            MessageLookupByLibrary.simpleMessage("Title Onboarding 1"),
        "titleOnboarding2":
            MessageLookupByLibrary.simpleMessage("Title Onboarding 2"),
        "titleOnboarding3":
            MessageLookupByLibrary.simpleMessage("Title Onboarding 3"),
        "titleOnboarding4":
            MessageLookupByLibrary.simpleMessage("Title Onboarding 4"),
        "yes": MessageLookupByLibrary.simpleMessage("Ya")
      };
}
