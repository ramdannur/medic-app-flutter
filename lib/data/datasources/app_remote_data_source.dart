import 'package:medicapp/core/network/app_network.dart';
import 'package:medicapp/data/models/login_response.dart';

abstract class AppRemoteDataSource {
  Future<String> login(String email, String password);
}

class AppRemoteDataSourceImpl implements AppRemoteDataSource {
  final AppNetworkClient client;

  AppRemoteDataSourceImpl({required this.client});

  @override
  Future<String> login(String email, String password) async {
    final response = await client.post(
      path: 'login',
      jsonMap: {
        'email': email,
        'password': password,
      },
    );

    return LoginResponse.fromJson(response.data).token ?? '';
  }
}
