import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:medicapp/common/models/user_model.dart';

class SessionPrefHelper {
  static SessionPrefHelper? _sessionPref;

  final _storage = const FlutterSecureStorage();

  factory SessionPrefHelper() => _sessionPref ?? SessionPrefHelper._instance();

  SessionPrefHelper._instance() {
    _sessionPref = this;
  }

  deleteUserSession() async {
    await _storage.delete(key: 'usersession');
  }

  Future<UserSessionModel?> loadUserSession() async {
    final res = await _storage.read(key: 'usersession');
    if (res == null) return null;

    return userSessionModelFromJson(res);
  }

  saveUserSession(UserSessionModel value) async {
    await _storage.write(key: 'usersession', value: userSessionModelToJson(value));
  }
}
