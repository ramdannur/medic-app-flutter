import 'package:medicapp/common/models/user_model.dart';
import 'package:medicapp/data/datasources/storage/session_pref_helper.dart';

abstract class AppLocalDataSource {
  Future<void> deleteUser();

  Future<UserSessionModel?> loadUser();

  Future<void> saveUser(UserSessionModel value);
}

class AppLocalDataSourceImpl implements AppLocalDataSource {
  final SessionPrefHelper sessionPref;

  final _sessionPref = SessionPrefHelper();

  AppLocalDataSourceImpl({required this.sessionPref});

  @override
  Future<void> deleteUser() async {
    await _sessionPref.deleteUserSession();
  }

  @override
  Future<UserSessionModel?> loadUser() async {
    return await _sessionPref.loadUserSession();
  }

  @override
  Future<void> saveUser(UserSessionModel value) async {
    await _sessionPref.saveUserSession(value);
  }
}
