class LoginResponse {
  String? error;
  String? token;

  LoginResponse({
    this.error,
    this.token,
  });

  factory LoginResponse.fromJson(Map<String, dynamic> json) => LoginResponse(
        error: json['error'],
        token: json['token'],
      );

  Map<String, dynamic> toJson() => {
        'error': error,
        'token': token,
      };
}
