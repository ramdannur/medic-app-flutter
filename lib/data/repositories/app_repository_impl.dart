import 'package:dartz/dartz.dart';
import 'package:medicapp/common/models/failure.dart';
import 'package:medicapp/common/models/user_model.dart';
import 'package:medicapp/data/datasources/app_local_data_source.dart';
import 'package:medicapp/data/datasources/app_remote_data_source.dart';
import 'package:medicapp/domain/repositories/app_repository.dart';

class AppRepositoryImpl implements AppRepository {
  final AppRemoteDataSource remoteDataSource;
  final AppLocalDataSource localDataSource;

  AppRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
  });

  @override
  Future<UserSessionModel?> loadSession() async {
    return await localDataSource.loadUser();
  }

  @override
  Future<Either<Failure, UserSessionModel>> login(String email, String password) async {
    try {
      final token = await remoteDataSource.login(email, password);
      final user = UserSessionModel(accessToken: token, email: email);

      localDataSource.saveUser(user);

      return Right(user);
    } catch (e) {
      return Left(ServerFailure(e.toString()));
    }
  }

  @override
  Future<Either<Failure, bool>> setUserSession(UserSessionModel? value) async {
    try {
      if (value == null) {
        await localDataSource.deleteUser();
      } else {
        await localDataSource.saveUser(value);
      }
      return const Right(true);
    } catch (e) {
      return Left(DatabaseFailure(''));
    }
  }
}
