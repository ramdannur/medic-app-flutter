import 'package:medicapp/common/models/user_model.dart';
import 'package:medicapp/domain/repositories/app_repository.dart';

class GetUserSession {
  final AppRepository repository;

  GetUserSession(this.repository);

  Future<UserSessionModel?> execute() {
    return repository.loadSession();
  }
}
