import 'package:dartz/dartz.dart';
import 'package:medicapp/common/models/failure.dart';
import 'package:medicapp/common/models/user_model.dart';
import 'package:medicapp/domain/repositories/app_repository.dart';

class SaveUserSession {
  final AppRepository repository;

  SaveUserSession(this.repository);

  Future<Either<Failure, bool>> execute(UserSessionModel? value) {
    return repository.setUserSession(value);
  }
}
