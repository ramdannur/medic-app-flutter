import 'package:dartz/dartz.dart';
import 'package:medicapp/common/models/failure.dart';
import 'package:medicapp/common/models/user_model.dart';
import 'package:medicapp/domain/repositories/app_repository.dart';

class LoginUsecase {
  final AppRepository repository;

  LoginUsecase(this.repository);

  Future<Either<Failure, UserSessionModel>> execute(String email, String password) {
    return repository.login(email, password);
  }
}
