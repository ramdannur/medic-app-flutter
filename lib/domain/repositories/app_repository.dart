import 'package:dartz/dartz.dart';
import 'package:medicapp/common/models/failure.dart';
import 'package:medicapp/common/models/user_model.dart';

abstract class AppRepository {
  Future<UserSessionModel?> loadSession();

  Future<Either<Failure, UserSessionModel>> login(String email, String password);

  Future<Either<Failure, bool>> setUserSession(UserSessionModel? value);
}
