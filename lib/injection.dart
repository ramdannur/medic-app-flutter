import 'package:get_it/get_it.dart';
import 'package:medicapp/core/network/app_network.dart';
import 'package:medicapp/data/datasources/app_local_data_source.dart';
import 'package:medicapp/data/datasources/app_remote_data_source.dart';
import 'package:medicapp/data/datasources/storage/session_pref_helper.dart';
import 'package:medicapp/data/repositories/app_repository_impl.dart';
import 'package:medicapp/domain/repositories/app_repository.dart';
import 'package:medicapp/domain/usecases/get_user_session.dart';
import 'package:medicapp/domain/usecases/login_usecase.dart';
import 'package:medicapp/domain/usecases/save_user_session.dart';
import 'package:medicapp/presentation/provider/auth_provider.dart';

final locator = GetIt.instance;

void init() {
  // provider
  locator.registerFactory(
    () => AuthProvider(
      locator(),
      locator(),
      locator(),
    ),
  );

  // use case
  locator.registerLazySingleton(() => LoginUsecase(locator()));
  locator.registerLazySingleton(() => SaveUserSession(locator()));
  locator.registerLazySingleton(() => GetUserSession(locator()));

  // repository
  locator.registerLazySingleton<AppRepository>(
    () => AppRepositoryImpl(
      remoteDataSource: locator(),
      localDataSource: locator(),
    ),
  );

  // data sources
  locator.registerLazySingleton<AppRemoteDataSource>(() => AppRemoteDataSourceImpl(client: locator()));
  locator.registerLazySingleton<AppLocalDataSource>(() => AppLocalDataSourceImpl(sessionPref: locator()));

  // helper
  locator.registerLazySingleton<SessionPrefHelper>(() => SessionPrefHelper());

  // external
  locator.registerLazySingleton(() => AppNetworkClient());
}
