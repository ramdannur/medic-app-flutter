import 'package:flutter/material.dart';

class AppColors {
  static const bluemain = Color.fromARGB(255, 24, 45, 88);
  static const bluemist = Color(0xffd8dfee);
  static const teal = Color(0xff009672);
  static const yellow = Color(0xfffad703);

  static const Color kDavysGrey = Color(0xFF4B5358);
  static const Color kGrey = Color(0xFF303030);
  static const Color kMikadoYellow = Color(0xFFffc300);
  static const Color kPrussianBlue = Color(0xFF003566);

  static const Color kRichBlack = Color(0xFF000814);
}
