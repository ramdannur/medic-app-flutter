import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:medicapp/core/style/app_color.dart';

class AppTheme {
  static final main = ThemeData(
    visualDensity: VisualDensity.adaptivePlatformDensity,
    scaffoldBackgroundColor: Colors.white,
    primaryColor: AppColors.kRichBlack,
    canvasColor: AppColors.kRichBlack,
    inputDecorationTheme: InputDecorationTheme(
      contentPadding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
      filled: true,
      fillColor: Colors.white,
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
        borderSide: BorderSide(
          color: Colors.grey[300]!,
        ),
      ),
      disabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
        borderSide: BorderSide(
          color: Colors.grey[300]!,
        ),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(AppColors.kRichBlack),
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        shape: MaterialStateProperty.all<OutlinedBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8), // Change the border radius as needed
          ),
        ),
        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
          const EdgeInsets.all(14.0), // Set the padding as needed
        ),
      ),
    ),
    outlinedButtonTheme: OutlinedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
        shape: MaterialStateProperty.all<OutlinedBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8), // Change the border radius as needed
          ),
        ),
        padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
          const EdgeInsets.all(14.0), // Set the padding as needed
        ),
      ),
    ),
    buttonTheme: ButtonThemeData(
      buttonColor: AppColors.kRichBlack,
      textTheme: ButtonTextTheme.primary,
      height: 54.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
    ),

    /// Default Text Theme Style
    textTheme: GoogleFonts.questrialTextTheme().copyWith(
      displayLarge: GoogleFonts.questrial(fontWeight: FontWeight.w600, fontSize: 18.0, color: AppColors.kRichBlack),
      displayMedium: GoogleFonts.questrial(fontWeight: FontWeight.w700, fontSize: 18.0, color: AppColors.kRichBlack),
      displaySmall: GoogleFonts.questrial(fontWeight: FontWeight.w600, fontSize: 16.0, color: AppColors.kRichBlack),
      headlineMedium: GoogleFonts.questrial(fontWeight: FontWeight.w600, fontSize: 14.0, color: AppColors.kRichBlack),
      headlineSmall: GoogleFonts.questrial(fontWeight: FontWeight.w500, fontSize: 12.0, color: AppColors.kRichBlack),
      titleLarge: GoogleFonts.questrial(fontWeight: FontWeight.w500, fontSize: 10.0, color: AppColors.kRichBlack),
      bodyLarge: GoogleFonts.questrial(fontSize: 14.0),
      bodyMedium: GoogleFonts.questrial(fontSize: 12.0, color: AppColors.kRichBlack),
      labelLarge: GoogleFonts.questrial(fontWeight: FontWeight.w600, fontSize: 16.0, color: AppColors.kRichBlack),
      bodySmall: GoogleFonts.questrial(fontWeight: FontWeight.w500, fontSize: 12.0),
    ),
    primaryTextTheme: const TextTheme(titleLarge: TextStyle(color: AppColors.kRichBlack, fontWeight: FontWeight.bold)),
    appBarTheme: const AppBarTheme(
      color: Colors.white,
      elevation: 0,
      iconTheme: IconThemeData(
        color: AppColors.bluemain,
      ),
      titleTextStyle: TextStyle(fontSize: 16, color: AppColors.kRichBlack, fontWeight: FontWeight.bold),
    ),
    dividerColor: Colors.grey[100],
  );
}
