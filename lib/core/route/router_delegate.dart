import 'package:flutter/material.dart';
import 'package:medicapp/presentation/pages/home_page.dart';
import 'package:medicapp/presentation/pages/login_page.dart';
import 'package:medicapp/presentation/pages/register_page.dart';
import 'package:medicapp/presentation/pages/splash_page.dart';
import 'package:medicapp/presentation/widgets/logout_page.dart';

class MyRouterDelegate extends RouterDelegate with ChangeNotifier, PopNavigatorRouterDelegateMixin {
  final GlobalKey<NavigatorState> _navigatorKey;

  String? selectedStory;

  List<Page> historyStack = [];

  bool? isLoggedIn;

  bool isRegister = false;

  bool isShowLogoutDialog = false;

  MyRouterDelegate() : _navigatorKey = GlobalKey<NavigatorState>() {
    // _init();
  }

  @override
  GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;
  List<Page> get _loggedInStack => [
        MaterialPage(
          key: const ValueKey(HomePage.routeName),
          child: HomePage(
            onShowLogout: () {
              isShowLogoutDialog = true;
              notifyListeners();
            },
          ),
        ),
        if (isShowLogoutDialog == true)
          LogoutPage(
            onClose: () {
              isShowLogoutDialog = false;
              notifyListeners();
            },
            onLogout: () {
              isShowLogoutDialog = false;
              isLoggedIn = false;
              notifyListeners();
            },
          ),
      ];

  List<Page> get _loggedOutStack => [
        MaterialPage(
          key: const ValueKey(LoginPage.routeName),
          child: LoginPage(
            onLogin: () {
              isLoggedIn = true;
              notifyListeners();
            },
            onRegister: () {
              isRegister = true;
              notifyListeners();
            },
          ),
        ),
        if (isRegister == true)
          MaterialPage(
            key: const ValueKey(RegisterPage.routeName),
            child: RegisterPage(
              onRegister: () {
                isRegister = false;
                notifyListeners();
              },
              onLogin: () {
                isRegister = false;
                notifyListeners();
              },
            ),
          ),
      ];

  List<Page> get _splashStack => [
        MaterialPage(
          key: const ValueKey(SplashPage.routeName),
          child: SplashPage(
            onResult: (result) {
              isLoggedIn = result;
              notifyListeners();
            },
          ),
        ),
      ];

  @override
  Widget build(BuildContext context) {
    if (isLoggedIn == null) {
      historyStack = _splashStack;
    } else if (isLoggedIn == true) {
      historyStack = _loggedInStack;
    } else {
      historyStack = _loggedOutStack;
    }
    return Navigator(
      key: navigatorKey,
      pages: historyStack,
      onPopPage: (route, result) {
        final didPop = route.didPop(result);
        if (!didPop) {
          return false;
        }

        isRegister = false;
        isShowLogoutDialog = false;
        selectedStory = null;
        notifyListeners();

        return true;
      },
    );
  }

  init() async {
    // isLoggedIn = authRead.isLoggedIn();
    // notifyListeners();
  }

  @override
  Future<void> setNewRoutePath(configuration) async {
    /* Do Nothing */
  }
}
