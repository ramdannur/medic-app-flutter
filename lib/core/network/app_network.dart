import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:medicapp/core/environment/app_environment.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

class AppNetworkClient {
  static final _prettyLogger = PrettyDioLogger(
    requestHeader: true,
    requestBody: true,
    responseBody: true,
    responseHeader: false,
    error: true,
    compact: true,
    maxWidth: 90,
  );

  String? baseurl = AppEnvironment.baseURL;

  final Dio _dio = Dio()
    ..interceptors.addAll([_prettyLogger])
    ..options.connectTimeout = const Duration(seconds: 20)
    ..options.receiveTimeout = const Duration(seconds: 15)
    ..options.headers.addAll({'Content-Type': 'application/json'});

  Future<Response> delete({
    Map<String, dynamic>? data,
    String? url,
    Map<String, dynamic>? customHeader,
    required String path,
    FormData? form,
    // jsonMap for sending raw json to server
    Map<String, dynamic>? jsonMap,
  }) async {
    customHeader ??= {};
    try {
      final varJson = form ?? jsonMap;

      final res = await _dio.delete((url ?? baseurl ?? '') + path,
          data: varJson != null
              ? jsonEncode(varJson)
              : data != null
                  ? FormData.fromMap(data)
                  : {},
          options: Options(
            headers: customHeader,
          ));

      return res;
    } on DioException catch (e) {
      throw _errorCatch(e);
    } catch (e) {
      debugPrint(e.toString());
      throw 'Something Went Wrong';
    }
  }

  Future<Response> get({
    Map<String, dynamic>? data,
    String? url,
    Map<String, dynamic>? customHeader,
    required String path,
  }) async {
    customHeader ??= {};
    try {
      final res = await _dio.get((url ?? baseurl ?? '') + path,
          queryParameters: data,
          options: Options(
            headers: customHeader,
          ));

      return res;
    } on DioException catch (e) {
      throw _errorCatch(e);
    } catch (e) {
      debugPrint(e.toString());
      throw 'Something Went Wrong';
    }
  }

  Future<Response> patch({
    Map<String, dynamic>? data,
    String? url,
    Map<String, dynamic>? customHeader,
    required String path,
    FormData? form,
    // jsonMap for sending raw json to server
    Map<String, dynamic>? jsonMap,
  }) async {
    customHeader ??= {};
    try {
      final varJson = form ?? jsonMap;

      final res = await _dio.patch((url ?? baseurl ?? '') + path,
          data: varJson != null
              ? jsonEncode(varJson)
              : data != null
                  ? FormData.fromMap(data)
                  : {},
          queryParameters: data,
          options: Options(
            headers: customHeader,
          ));

      return res;
    } on DioException catch (e) {
      throw _errorCatch(e);
    } catch (e) {
      debugPrint(e.toString());
      throw 'Something Went Wrong';
    }
  }

  Future<Response> post({
    Map<String, dynamic>? data,
    String? url,
    Map<String, dynamic>? customHeader,
    required String path,
    FormData? form,
    // jsonMap for sending raw json to server
    Map<String, dynamic>? jsonMap,
  }) async {
    customHeader ??= {};
    try {
      final res = await _dio.post(
        (url ?? baseurl ?? '') + path,
        data: form ?? jsonMap ?? (data != null ? FormData.fromMap(data) : {}),
        options: Options(headers: customHeader, followRedirects: false),
      );
      return res;
    } on DioException catch (e) {
      throw _errorCatch(e);
    } catch (e) {
      debugPrint(e.toString());
      throw 'Something Went Wrong';
    }
  }

  Future<Response> put({
    Map<String, dynamic>? data,
    String? url,
    Map<String, dynamic>? customHeader,
    required String path,
    FormData? form,
    // jsonMap for sending raw json to server
    Map<String, dynamic>? jsonMap,
  }) async {
    customHeader ??= {};
    try {
      final varJson = form ?? jsonMap;

      final res = await _dio.put((url ?? baseurl ?? '') + path,
          data: varJson != null
              ? jsonEncode(varJson)
              : data != null
                  ? FormData.fromMap(data)
                  : {},
          options: Options(
            headers: customHeader,
          ));

      return res;
    } on DioException catch (e) {
      throw _errorCatch(e);
    } catch (e) {
      debugPrint(e.toString());
      throw 'Something Went Wrong';
    }
  }

  String _errorCatch(DioException e) {
    if (e.response != null) {
      debugPrint('Error CALLING ${e.requestOptions.path}');

      debugPrint('Error Status Code ${e.response?.statusCode.toString()}');
      debugPrint('Error Response ${e.response?.data.toString()}');

      try {
        final Map<String, dynamic> jsonData = e.response?.data;
        if (jsonData.containsKey('error') && jsonData['error'] is String && jsonData['error'] != null) {
          return jsonData['error'];
        } else if (jsonData.containsKey('message') && jsonData['message'] is String && jsonData['message'] != null) {
          return jsonData['message'];
        }
      } catch (error) {
        debugPrint('Error parsing error ${e.response?.data}');
        debugPrint(error.toString());
      }
    } else {
      debugPrint('CALLING ${e.requestOptions.toString()}');
      debugPrint(e.message);
    }

    return 'Server Error, try again later';
  }
}
