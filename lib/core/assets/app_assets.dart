class AppAssets {
  static const logoApp = 'assets/logo/logo.png';
  static const imgBannerMedic = 'assets/img/img_banner_medic.png';
  static const imgBannerInfo = 'assets/img/img_banner_info.png';
  static const imgBannerVaccine = 'assets/img/img_banner_vaccine.png';
  static const imgBannerTrack = 'assets/img/img_banner_track.png';
  static const imgFooterHome = 'assets/img/img_footer_home.png';
  static const imgProduct = 'assets/img/img_products.png';
  static const imgServices = 'assets/img/img_services.png';
}
