class AppEnvironment {
  static const _flavor = String.fromEnvironment('CURRENT_FLAVOR', defaultValue: '');

  static final AppEnvironment _instance = AppEnvironment._();

  static String? baseURL;

  static String? domain;
  static String? schemedeeplink;
  factory AppEnvironment() {
    return _instance;
  }
  AppEnvironment._();

  static init() {
    if (_flavor == 'prod') {
      baseURL = 'https://reqres.in/api/';
      domain = 'domain.prod';
      schemedeeplink = 'deeplinkscheme';
    }

    if (_flavor == 'dev') {
      baseURL = 'https://reqres.in/api/';
      domain = 'domain.dev';
      schemedeeplink = 'deeplinkschemedev';
    }

    if (_flavor == 'staging') {
      baseURL = 'https://reqres.in/api/';
      domain = 'domain.dev';
      schemedeeplink = 'deeplinkschemestaging';
    }
  }
}
