import 'package:flutter/foundation.dart';
import 'package:medicapp/common/models/setmodel.dart';
import 'package:medicapp/common/models/user_model.dart';
import 'package:medicapp/domain/usecases/get_user_session.dart';
import 'package:medicapp/domain/usecases/login_usecase.dart';
import 'package:medicapp/domain/usecases/save_user_session.dart';

class AuthProvider extends ChangeNotifier {
  final LoginUsecase _loginUsecase;
  final SaveUserSession _saveUserSession;
  final GetUserSession _getUserSession;

  final SetModel<UserSessionModel> _userSession = SetModel<UserSessionModel>();

  bool _obsecureState = false;

  AuthProvider(this._loginUsecase, this._saveUserSession, this._getUserSession);

  get obsecureState => _obsecureState;

  get userSession => _userSession;

  Future<bool> checkSession() async {
    _userSession.loading = true;
    notifyListeners();

    final result = await _getUserSession.execute();

    _userSession.item = result;
    _userSession.loading = false;
    notifyListeners();

    if (_userSession.item != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> logout() async {
    var isSuccess = false;
    _userSession.loading = true;
    notifyListeners();

    final result = await _saveUserSession.execute(null);

    result.fold(
      (failure) {
        _userSession.error = failure.message;
        _userSession.loading = false;
        notifyListeners();
      },
      (user) {
        _userSession.item = null;
        _userSession.loading = false;
        isSuccess = true;
        notifyListeners();
      },
    );

    _userSession.loading = false;
    notifyListeners();

    return isSuccess;
  }

  Future<void> setObsecureState(bool isShow) async {
    _obsecureState = isShow;
    notifyListeners();
  }

  Future<UserSessionModel?> submitLogin(String email, String password) async {
    _userSession.error = null;
    _userSession.loading = true;
    notifyListeners();

    final result = await _loginUsecase.execute(email, password);

    return result.fold((failure) {
      _userSession.error = failure.message;
      _userSession.loading = false;
      notifyListeners();
      return null;
    }, (user) {
      userSession.item = user;
      _userSession.loading = false;
      notifyListeners();

      return user;
    });
  }
}
