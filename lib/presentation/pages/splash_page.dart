import 'package:flutter/material.dart';
import 'package:medicapp/common/widgets/media/local_image_widget.dart';
import 'package:medicapp/core/assets/app_assets.dart';
import 'package:medicapp/core/style/app_text_style.dart';
import 'package:medicapp/generated/l10n.dart';
import 'package:medicapp/presentation/provider/auth_provider.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatefulWidget {
  static const routeName = '/splash';

  final Function(bool) onResult;

  const SplashPage({
    super.key,
    required this.onResult,
  });

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              width: 200,
              child: LocalImage(
                AppAssets.logoApp,
                boxFit: BoxFit.fitWidth,
              ),
            ),
            const SizedBox(
              height: 80,
            ),
            Text(
              S.of(context).appName,
              style: AppTextStyle.xlExtraBold().copyWith(fontSize: 26),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await Future.delayed(const Duration(seconds: 2));

      final isLoggedIn = await context.read<AuthProvider>().checkSession();
      print('isLoggedIn $isLoggedIn');
      widget.onResult(isLoggedIn);
    });
    super.initState();
  }
}
