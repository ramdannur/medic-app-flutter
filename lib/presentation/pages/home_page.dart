import 'package:flutter/material.dart';
import 'package:medicapp/common/widgets/media/local_image_widget.dart';
import 'package:medicapp/core/assets/app_assets.dart';
import 'package:medicapp/core/style/app_color.dart';
import 'package:medicapp/core/style/app_text_style.dart';
import 'package:medicapp/presentation/widgets/item_product.dart';
import 'package:medicapp/presentation/widgets/item_service.dart';

class HomePage extends StatefulWidget {
  static const routeName = '/home';

  final Function() onShowLogout;

  const HomePage({
    Key? key,
    required this.onShowLogout,
  });

  @override
  State<HomePage> createState() => _HomePageState();
}

enum HomeType { movie, tvSeries }

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        backgroundColor: Colors.white,
        child: Column(
          children: [
            const UserAccountsDrawerHeader(
              currentAccountPicture: CircleAvatar(
                backgroundImage: NetworkImage('https://media.sproutsocial.com/uploads/2022/06/profile-picture.jpeg'),
              ),
              accountName: Text('Angga Praja'),
              accountEmail: Text('Membership BBLK'),
            ),
            ListTile(
              leading: const Icon(Icons.person),
              title: const Text('Profile Saya'),
              onTap: () {
                Navigator.pop(context);
                setState(() {});
              },
            ),
            ListTile(
              leading: const Icon(Icons.settings),
              title: const Text('Pengaturan'),
              onTap: () {
                Navigator.pop(context);
                setState(() {});
              },
            ),
            ListTile(
              onTap: () {
                widget.onShowLogout();
              },
              leading: const Icon(Icons.logout_outlined),
              title: const Text('Logout'),
              iconColor: Colors.red,
              textColor: Colors.red,
            ),
          ],
        ),
      ),
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.shopping_cart),
          ),
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.notifications),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: _buildContent(),
        ),
      ),
    );
  }

  List<Widget> _buildContent() {
    return [
      _viewBannerInfo(),
      _viewBannerVaccine(),
      _viewBannerTrack(),
      _listProducts(),
      _listTypeService(),
      _viewFooterHome(),
    ];
  }

  Widget _listProducts() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 150,
            child: ListView.separated(
              padding: const EdgeInsets.all(8.0),
              itemCount: 3,
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              physics: const ClampingScrollPhysics(),
              itemBuilder: (context, index) {
                return SizedBox(
                  width: 150,
                  child: ItemProduct(),
                );
              },
              separatorBuilder: (context, index) {
                return const SizedBox(
                  width: 12,
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _listTypeService() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Pilih Tipe Layanan Kesehatan Anda',
            style: AppTextStyle.xlReguler(),
          ),
          const SizedBox(
            height: 12,
          ),
          ListView.separated(
            itemCount: 3,
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.all(0),
            shrinkWrap: true,
            physics: const ClampingScrollPhysics(),
            itemBuilder: (context, index) {
              return SizedBox(
                height: 140,
                child: ItemService(),
              );
            },
            separatorBuilder: (context, index) {
              return const SizedBox(
                height: 12,
              );
            },
          )
        ],
      ),
    );
  }

  Widget _viewBannerInfo() {
    return Stack(
      children: [
        const LocalImage(
          AppAssets.imgBannerInfo,
          boxFit: BoxFit.fitWidth,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(40.0, 60.0, 140.0, 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                text: TextSpan(
                  style: AppTextStyle.xlMedium(),
                  children: const <TextSpan>[
                    TextSpan(text: 'Solusi, '),
                    TextSpan(text: 'Kesehatan Anda', style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                'Update informasi seputar kesehatan semua bisa disini !',
                style: AppTextStyle.sReguler(),
                textAlign: TextAlign.start,
              ),
              const SizedBox(
                height: 8,
              ),
              SizedBox(
                height: 30,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      AppColors.bluemain,
                    ),
                    padding: MaterialStateProperty.all(
                      const EdgeInsets.symmetric(horizontal: 10),
                    ),
                  ),
                  onPressed: () {},
                  child: Text(
                    'Selengkapnya',
                    textAlign: TextAlign.center,
                    style: AppTextStyle.sReguler(color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _viewBannerTrack() {
    return Stack(
      children: [
        const LocalImage(
          AppAssets.imgBannerTrack,
          boxFit: BoxFit.fitWidth,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(160.0, 15.0, 40.0, 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                text: TextSpan(
                  style: AppTextStyle.xlMedium(),
                  children: const <TextSpan>[
                    TextSpan(text: 'Track Pemeriksaan'),
                  ],
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                'Kamu dapat mengecek progress pemeriksaanmu disini',
                style: AppTextStyle.sReguler(),
                textAlign: TextAlign.start,
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Text(
                    'Track',
                    textAlign: TextAlign.center,
                    style: AppTextStyle.mSemiBold(color: AppColors.bluemain),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  const Icon(
                    Icons.arrow_forward,
                    color: AppColors.bluemain,
                    size: 16,
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _viewBannerVaccine() {
    return Stack(
      children: [
        const LocalImage(
          AppAssets.imgBannerVaccine,
          boxFit: BoxFit.fitWidth,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(40.0, 60.0, 160.0, 30.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RichText(
                text: TextSpan(
                  style: AppTextStyle.xlMedium(),
                  children: const <TextSpan>[
                    TextSpan(text: 'Layanan Khusus'),
                  ],
                ),
              ),
              const SizedBox(
                height: 8,
              ),
              Text(
                'Tes Covid 19, Cegah Corona Sedini Mungkin',
                style: AppTextStyle.sReguler(),
                textAlign: TextAlign.start,
              ),
              const SizedBox(
                height: 8,
              ),
              Row(
                children: [
                  Text(
                    'Daftar Tes',
                    textAlign: TextAlign.center,
                    style: AppTextStyle.mSemiBold(color: AppColors.bluemain),
                  ),
                  const SizedBox(
                    width: 8,
                  ),
                  const Icon(
                    Icons.arrow_forward,
                    color: AppColors.bluemain,
                    size: 16,
                  ),
                ],
              )
            ],
          ),
        )
      ],
    );
  }

  Widget _viewFooterHome() {
    return Stack(
      children: [
        const LocalImage(
          AppAssets.imgFooterHome,
          boxFit: BoxFit.fitWidth,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(18.0, 76.0, 18.0, 0.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  'Ingin mendapat update dari kami ?',
                  textAlign: TextAlign.start,
                  style: AppTextStyle.lSemiBold(color: Colors.white),
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      'Daftar Tes',
                      textAlign: TextAlign.end,
                      style: AppTextStyle.mSemiBold(color: Colors.white),
                    ),
                    const SizedBox(
                      width: 8,
                    ),
                    const Icon(
                      Icons.arrow_forward,
                      color: Colors.white,
                      size: 16,
                    ),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
