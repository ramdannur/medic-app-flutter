import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:medicapp/common/widgets/input/formtitle.dart';
import 'package:medicapp/common/widgets/media/local_image_widget.dart';
import 'package:medicapp/core/assets/app_assets.dart';
import 'package:medicapp/core/style/app_color.dart';
import 'package:medicapp/core/style/app_text_style.dart';
import 'package:medicapp/generated/l10n.dart';
import 'package:medicapp/presentation/provider/auth_provider.dart';
import 'package:provider/provider.dart';
import 'package:reactive_forms/reactive_forms.dart';

class LoginPage extends StatefulWidget {
  static const routeName = '/login';

  final Function() onLogin;
  final Function() onRegister;

  const LoginPage({
    super.key,
    required this.onLogin,
    required this.onRegister,
  });

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final form = fb.group({
    'email': ['', Validators.required],
    'password': ['', Validators.required, Validators.minLength(8)],
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 38.0, left: 16.0),
                child: RichText(
                  text: TextSpan(
                    style: AppTextStyle.xlMedium().copyWith(fontSize: 28),
                    children: const <TextSpan>[
                      TextSpan(text: 'Hai, '),
                      TextSpan(text: 'Selamat Datang', style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0, top: 4.0),
                child: Text(
                  'Silahkan login untuk melanjutkan',
                  style: AppTextStyle.sReguler(),
                  textAlign: TextAlign.center,
                ),
              ),
              const Align(
                alignment: Alignment.centerRight,
                child: LocalImage(
                  AppAssets.imgBannerMedic,
                  boxFit: BoxFit.fitWidth,
                ),
              ),
              Container(transform: Matrix4.translationValues(0.0, -30.0, 0.0), child: _sectionForm()),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 6),
                      child: Text(
                        'Belum punya akun?',
                        style: AppTextStyle.sMedium(color: AppColors.kGrey),
                      ),
                    ),
                    GestureDetector(
                      onTap: () => widget.onRegister(),
                      child: Text(
                        'Daftar Sekarang',
                        style: AppTextStyle.sBold(color: AppColors.bluemain),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30.0),
                child: Center(
                  child: Text(
                    '© SILK. all right reserved',
                    style: AppTextStyle.sMedium(color: AppColors.kGrey),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _sectionForm() {
    return Consumer<AuthProvider>(builder: (context, data, child) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: SlideInDown(
          child: ReactiveForm(
            formGroup: form,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                FormTitle(
                  S.of(context).email,
                  color: AppColors.bluemain,
                ),
                ReactiveTextField(
                  onTapOutside: (event) => FocusScope.of(context).requestFocus(FocusNode()),
                  keyboardType: TextInputType.number,
                  formControlName: 'email',
                  decoration: InputDecoration(
                    hintText: S.of(context).masukkanEmail,
                  ),
                  textInputAction: TextInputAction.next,
                  validationMessages: {
                    'required': (error) => S.of(context).required,
                  },
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                const SizedBox(height: 12),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    FormTitle(S.of(context).kataSandi),
                    GestureDetector(
                      onTap: () => {},
                      child: Text(
                        'Lupa Password Anda?',
                        style: AppTextStyle.sBold(color: AppColors.bluemain),
                      ),
                    )
                  ],
                ),
                SlideInLeft(
                  child: ReactiveTextField(
                    formControlName: 'password',
                    decoration: InputDecoration(
                      hintText: S.of(context).masukkanKataSandi,
                      suffixIcon: GestureDetector(
                        onTap: () async {
                          await Provider.of<AuthProvider>(context, listen: false).setObsecureState(!data.obsecureState);
                        },
                        child: Icon(
                          data.obsecureState ? Icons.visibility : Icons.visibility_off,
                          size: 18,
                        ),
                      ),
                    ),
                    obscureText: data.obsecureState,
                    maxLines: 1,
                    validationMessages: {'required': (error) => 'Password harus terisi', 'minLength': (error) => 'Password kurang dari 8 character'},
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                ),
                const SizedBox(height: 20),
                if (data.userSession.error != null)
                  Padding(
                    padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                    child: Center(
                      child: Text(
                        data.userSession.error ?? '',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Colors.red),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                const SizedBox(height: 20),
                data.userSession.loading == true ? const Center(child: CircularProgressIndicator()) : _submitButton()
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget _submitButton() {
    return SizedBox(
      width: double.infinity,
      child: ReactiveFormConsumer(
        builder: (context, form, child) {
          final isFormValid = form.valid;
          return ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(
                isFormValid ? AppColors.bluemain : Colors.grey,
              ),
            ),
            onPressed: isFormValid
                ? () async {
                    final result = await Provider.of<AuthProvider>(context, listen: false).submitLogin(
                      form.control('email').value,
                      form.control('password').value,
                    );

                    if (result != null) {
                      widget.onLogin();
                    }
                  }
                : null,
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    S.of(context).login,
                    textAlign: TextAlign.center,
                  ),
                ),
                const Icon(Icons.arrow_forward),
              ],
            ),
          );
        },
      ),
    );
  }
}
