import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:medicapp/common/widgets/input/formtitle.dart';
import 'package:medicapp/common/widgets/media/local_image_widget.dart';
import 'package:medicapp/core/assets/app_assets.dart';
import 'package:medicapp/core/style/app_color.dart';
import 'package:medicapp/core/style/app_text_style.dart';
import 'package:medicapp/generated/l10n.dart';
import 'package:medicapp/presentation/provider/auth_provider.dart';
import 'package:provider/provider.dart';
import 'package:reactive_forms/reactive_forms.dart';

class RegisterPage extends StatefulWidget {
  static const routeName = '/register';

  final Function() onLogin;
  final Function() onRegister;

  const RegisterPage({
    super.key,
    required this.onLogin,
    required this.onRegister,
  });

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final form = fb.group({
    'firstname': ['', Validators.required],
    'lastname': ['', Validators.required],
    'ktp': ['', Validators.required],
    'email': ['', Validators.required],
    'phone': ['', Validators.required],
    'password': ['', Validators.required, Validators.minLength(8)],
    'confirm_password': ['', Validators.required, Validators.minLength(8)],
  });

  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 38.0, left: 16.0),
                child: RichText(
                  text: TextSpan(
                    style: AppTextStyle.xlMedium().copyWith(fontSize: 28),
                    children: const <TextSpan>[
                      TextSpan(text: 'Hai, '),
                      TextSpan(text: 'Selamat Datang', style: TextStyle(fontWeight: FontWeight.bold)),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16.0, top: 4.0),
                child: Text(
                  'Silahkan login untuk melanjutkan',
                  style: AppTextStyle.sReguler(),
                  textAlign: TextAlign.center,
                ),
              ),
              const Align(
                alignment: Alignment.centerRight,
                child: LocalImage(
                  AppAssets.imgBannerMedic,
                  boxFit: BoxFit.fitWidth,
                ),
              ),
              Container(transform: Matrix4.translationValues(0.0, -30.0, 0.0), child: _sectionForm()),
              Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 6),
                      child: Text(
                        'Sudah punya akun?',
                        style: AppTextStyle.sMedium(color: AppColors.kGrey),
                      ),
                    ),
                    GestureDetector(
                      onTap: () => widget.onLogin(),
                      child: Text(
                        'Login Sekarang',
                        style: AppTextStyle.sBold(color: AppColors.bluemain),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30.0),
                child: Center(
                  child: Text(
                    '© SILK. all right reserved',
                    style: AppTextStyle.sMedium(color: AppColors.kGrey),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _sectionForm() {
    return Consumer<AuthProvider>(builder: (context, data, child) {
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: SlideInDown(
          child: ReactiveForm(
            key: formKey,
            formGroup: form,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FormTitle(
                            S.of(context).namaDepan,
                            color: AppColors.bluemain,
                          ),
                          ReactiveTextField(
                            onTapOutside: (event) => FocusScope.of(context).requestFocus(FocusNode()),
                            keyboardType: TextInputType.name,
                            formControlName: 'firstname',
                            decoration: InputDecoration(
                              hintText: S.of(context).masukkanNamaDepan,
                            ),
                            textInputAction: TextInputAction.next,
                            validationMessages: {
                              'required': (error) => S.of(context).required,
                            },
                            style: Theme.of(context).textTheme.bodyLarge,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          FormTitle(
                            S.of(context).namaBelakang,
                            color: AppColors.bluemain,
                          ),
                          ReactiveTextField(
                            onTapOutside: (event) => FocusScope.of(context).requestFocus(FocusNode()),
                            keyboardType: TextInputType.name,
                            formControlName: 'lastname',
                            decoration: InputDecoration(
                              hintText: S.of(context).masukkanNamaBelakang,
                            ),
                            textInputAction: TextInputAction.next,
                            validationMessages: {
                              'required': (error) => S.of(context).required,
                            },
                            style: Theme.of(context).textTheme.bodyLarge,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 12),
                FormTitle(
                  S.of(context).noKtp,
                  color: AppColors.bluemain,
                ),
                ReactiveTextField(
                  onTapOutside: (event) => FocusScope.of(context).requestFocus(FocusNode()),
                  keyboardType: TextInputType.number,
                  formControlName: 'ktp',
                  decoration: InputDecoration(
                    hintText: S.of(context).masukkanNoKtp,
                  ),
                  textInputAction: TextInputAction.next,
                  validationMessages: {
                    'required': (error) => S.of(context).required,
                  },
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                const SizedBox(height: 12),
                FormTitle(
                  S.of(context).email,
                  color: AppColors.bluemain,
                ),
                ReactiveTextField(
                  onTapOutside: (event) => FocusScope.of(context).requestFocus(FocusNode()),
                  keyboardType: TextInputType.emailAddress,
                  formControlName: 'email',
                  decoration: InputDecoration(
                    hintText: S.of(context).masukkanEmail,
                  ),
                  textInputAction: TextInputAction.next,
                  validationMessages: {
                    'required': (error) => S.of(context).required,
                  },
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                const SizedBox(height: 12),
                FormTitle(
                  S.of(context).noTelpon,
                  color: AppColors.bluemain,
                ),
                ReactiveTextField(
                  onTapOutside: (event) => FocusScope.of(context).requestFocus(FocusNode()),
                  keyboardType: TextInputType.number,
                  formControlName: 'phone',
                  decoration: InputDecoration(
                    hintText: S.of(context).masukkanTelp,
                  ),
                  textInputAction: TextInputAction.next,
                  validationMessages: {
                    'required': (error) => S.of(context).required,
                  },
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                const SizedBox(height: 12),
                FormTitle(S.of(context).kataSandi),
                SlideInLeft(
                  child: ReactiveTextField(
                    formControlName: 'password',
                    decoration: InputDecoration(
                      hintText: S.of(context).masukkanKataSandi,
                      suffixIcon: GestureDetector(
                        onTap: () async {
                          await Provider.of<AuthProvider>(context, listen: false).setObsecureState(!data.obsecureState);
                        },
                        child: Icon(
                          data.obsecureState ? Icons.visibility : Icons.visibility_off,
                          size: 18,
                        ),
                      ),
                    ),
                    obscureText: data.obsecureState,
                    maxLines: 1,
                    validationMessages: {'required': (error) => 'Password harus terisi', 'minLength': (error) => 'Password kurang dari 8 character'},
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                ),
                const SizedBox(height: 12),
                FormTitle(S.of(context).konfirmasiKataSandi),
                SlideInLeft(
                  child: ReactiveTextField(
                    formControlName: 'confirm_password',
                    decoration: InputDecoration(
                      hintText: S.of(context).masukkanKataSandi,
                      suffixIcon: GestureDetector(
                        onTap: () async {
                          await Provider.of<AuthProvider>(context, listen: false).setObsecureState(!data.obsecureState);
                        },
                        child: Icon(
                          data.obsecureState ? Icons.visibility : Icons.visibility_off,
                          size: 18,
                        ),
                      ),
                    ),
                    obscureText: data.obsecureState,
                    maxLines: 1,
                    validationMessages: {'required': (error) => 'Password harus terisi', 'minLength': (error) => 'Password kurang dari 8 character'},
                    style: Theme.of(context).textTheme.bodyLarge,
                  ),
                ),
                const SizedBox(height: 20),
                if (data.userSession.error != null)
                  Padding(
                    padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                    child: Center(
                      child: Text(
                        data.userSession.error ?? '',
                        style: Theme.of(context).textTheme.titleMedium?.copyWith(color: Colors.red),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                const SizedBox(height: 20),
                data.userSession.loading == true ? const Center(child: CircularProgressIndicator()) : _submitButton()
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget _submitButton() {
    return SizedBox(
      width: double.infinity,
      child: ReactiveFormConsumer(
        builder: (context, form, child) {
          final isFormValid = form.valid;
          return ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(
                isFormValid ? AppColors.bluemain : Colors.grey,
              ),
            ),
            onPressed: isFormValid
                ? () async {
                    if (formKey.currentState!.validate()) {
                      // TODO: Register

                      widget.onRegister();
                    }
                  }
                : null,
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    S.of(context).register,
                    textAlign: TextAlign.center,
                  ),
                ),
                const Icon(Icons.arrow_forward),
              ],
            ),
          );
        },
      ),
    );
  }
}
