import 'package:flutter/material.dart';
import 'package:medicapp/common/widgets/media/local_image_widget.dart';
import 'package:medicapp/core/assets/app_assets.dart';
import 'package:medicapp/core/style/app_color.dart';
import 'package:medicapp/core/style/app_shadow.dart';
import 'package:medicapp/core/style/app_text_style.dart';

class ItemService extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.0),
        color: Colors.white,
        border: Border.all(
          width: 1.0,
          color: Colors.white,
        ),
        boxShadow: AppShadow.primary,
      ),
      width: double.infinity,
      child: Row(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'PCR Swab Test (Drive Thru) Hasil 1 Hari Kerja',
                    style: AppTextStyle.lSemiBold(),
                  ),
                  Expanded(
                    child: Text(
                      'Rp. 10.000',
                      style: AppTextStyle.lReguler(color: Colors.orange),
                    ),
                  ),
                  Text(
                    'Lenmarc Surabaya',
                    style: AppTextStyle.sReguler(color: AppColors.bluemain),
                  ),
                  Text(
                    'Dukuh Pakis, Surabaya',
                    style: AppTextStyle.sReguler(color: AppColors.kGrey),
                  )
                ],
              ),
            ),
          ),
          const ClipRRect(
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(8.0),
              bottomRight: Radius.circular(8.0),
            ),
            child: LocalImage(AppAssets.imgServices),
          )
        ],
      ),
    );
  }
}
