import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medicapp/common/utils/color_extensions.dart';
import 'package:medicapp/common/utils/shape_border_type.dart';
import 'package:medicapp/generated/l10n.dart';
import 'package:medicapp/presentation/provider/auth_provider.dart';
import 'package:medicapp/presentation/widgets/shape_dialog.dart';
import 'package:provider/provider.dart';

class LogoutPage extends Page {
  static const String routeName = 'LogoutPage';
  final Function() onClose;
  final Function() onLogout;

  const LogoutPage({
    required this.onClose,
    required this.onLogout,
  }) : super(key: const ValueKey('LogoutPage'));

  @override
  String get name => routeName;

  @override
  Route createRoute(BuildContext context) {
    return CupertinoDialogRoute(
      settings: this,
      barrierDismissible: true,
      barrierColor: Colors.black87,
      builder: (BuildContext context) => ConfirmDialog(
        title: S.of(context).confirmation,
        description: S.of(context).msgLogoutConfirmation,
        colorCode: Colors.blue.toHex(),
        shapeBorderType: ShapeBorderType.ROUNDED,
        onNegativeAction: () {
          onClose();
        },
        onPositiveAction: () async {
          onClose();

          final scaffoldMessenger = ScaffoldMessenger.of(context);
          final appLocalizations = S.of(context);

          scaffoldMessenger.showSnackBar(
            SnackBar(
              content: Text('${appLocalizations.keluar} ...'),
            ),
          );

          final authRead = context.read<AuthProvider>();
          final result = await authRead.logout();
          if (result) {
            onLogout();
          } else {
            scaffoldMessenger.showSnackBar(
              SnackBar(
                content: Text(appLocalizations.msgFailedToLogout),
              ),
            );
          }
        },
      ),
      context: context,
    );
  }
}
