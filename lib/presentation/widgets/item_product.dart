import 'package:flutter/material.dart';
import 'package:medicapp/common/widgets/media/local_image_widget.dart';
import 'package:medicapp/core/assets/app_assets.dart';
import 'package:medicapp/core/style/app_shadow.dart';
import 'package:medicapp/core/style/app_text_style.dart';

class ItemProduct extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6.0),
        color: Colors.white,
        border: Border.all(
          width: 1.0,
          color: Colors.white,
        ),
        boxShadow: AppShadow.primary,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 6.0),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Expanded(child: Center(child: LocalImage(AppAssets.imgProduct))),
          const SizedBox(
            height: 8.0,
          ),
          Text(
            'Suntik Steril',
            style: AppTextStyle.mSemiBold(),
          ),
          Text(
            'Rp. 10.000',
            style: AppTextStyle.mReguler(color: Colors.orange),
          ),
        ],
      ),
    );
  }
}
