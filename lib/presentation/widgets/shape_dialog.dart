/*
 * Copyright 2020 Cagatay Ulusoy (Ulus Oy Apps). All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import 'package:flutter/material.dart';
import 'package:medicapp/common/utils/color_extensions.dart';
import 'package:medicapp/common/utils/shape_border_type.dart';
import 'package:medicapp/generated/l10n.dart';

class ConfirmDialog extends StatelessWidget {
  final String title;
  final String description;
  final String colorCode;
  final ShapeBorderType shapeBorderType;
  final Function() onNegativeAction;
  final Function() onPositiveAction;

  const ConfirmDialog({
    super.key,
    required this.title,
    required this.description,
    required this.colorCode,
    required this.shapeBorderType,
    required this.onNegativeAction,
    required this.onPositiveAction,
  });

  @override
  Widget build(BuildContext context) {
    final borderType = shapeBorderType.stringRepresentation().toUpperCase();
    return Center(
      child: FittedBox(
        child: Container(
          width: 250,
          color: Colors.white,
          child: Material(
            color: Colors.white,
            child: Column(
              children: [
                _bar(borderType),
                const SizedBox(
                  height: 16.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: Text(description),
                ),
                const SizedBox(
                  height: 8.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      InkWell(
                        onTap: () => onNegativeAction(),
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(S.of(context).no),
                        ),
                      ),
                      const SizedBox(
                        width: 16.0,
                      ),
                      InkWell(
                        onTap: () => onPositiveAction(),
                        child: Container(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            S.of(context).yes,
                            style: const TextStyle(color: Colors.red),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _bar(String borderType) {
    final color = colorCode.hexToColor();
    return Container(
      color: color,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              title,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
          ),
          IconButton(
            icon: const Icon(
              Icons.close,
              color: Colors.white,
              size: 18,
            ),
            onPressed: () => onNegativeAction(),
          ),
        ],
      ),
    );
  }
}
