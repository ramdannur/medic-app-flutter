import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:medicapp/common/utils/utils.dart';
import 'package:medicapp/core/environment/app_environment.dart';
import 'package:medicapp/core/route/router_delegate.dart';
import 'package:medicapp/core/style/app_theme.dart';
import 'package:medicapp/generated/l10n.dart';
import 'package:medicapp/injection.dart' as di;
import 'package:medicapp/presentation/provider/auth_provider.dart';
import 'package:provider/provider.dart';

void main() {
  AppEnvironment.init();
  di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => di.locator<AuthProvider>(),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: AppTheme.main,
        navigatorObservers: [routeObserver],
        home: Router(
          routerDelegate: MyRouterDelegate(),
          backButtonDispatcher: RootBackButtonDispatcher(),
        ),
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: S.delegate.supportedLocales,
      ),
    );
  }
}
