import 'package:flutter/widgets.dart';

final RouteObserver<ModalRoute> routeObserver = RouteObserver<ModalRoute>();

int boolToInt(bool val) => val ? 1 : 0;

bool intToBool(int val) => val == 1 ? true : false;
