import 'dart:convert';

UserSessionModel userSessionModelFromJson(String str) => UserSessionModel.fromJson(json.decode(str));

String userSessionModelToJson(UserSessionModel data) => json.encode(data.toJson());

class UserSessionModel {
  String accessToken;
  String email;

  UserSessionModel({
    required this.accessToken,
    required this.email,
  });

  factory UserSessionModel.fromJson(Map<String, dynamic> json) => UserSessionModel(
        accessToken: json['access_token'],
        email: json['email'],
      );

  UserSessionModel copyWith({
    String? accessToken,
    String? email,
  }) {
    return UserSessionModel(
      accessToken: accessToken ?? this.accessToken,
      email: email ?? this.email,
    );
  }

  Map<String, dynamic> toJson() => {
        'access_token': accessToken,
        'email': email,
      };
}
