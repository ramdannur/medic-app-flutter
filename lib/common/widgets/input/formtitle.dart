import 'package:flutter/material.dart';

class FormTitle extends StatelessWidget {
  final String title;
  final String? subTitle;
  final Color? color;

  const FormTitle(this.title, {this.subTitle, this.color, super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.labelLarge?.copyWith(color: color),
          ),
          if (subTitle != null) ...[
            const SizedBox(
              height: 8,
            ),
            Text(
              subTitle ?? '',
              style: Theme.of(context).textTheme.headlineSmall,
            ),
          ]
        ],
      ),
    );
  }
}
