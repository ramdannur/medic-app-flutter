import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LocalImage extends StatelessWidget {
  final String path;
  final double? width;
  final double? height;
  final BoxFit boxFit;
  final Color? svgColor;

  const LocalImage(
    this.path, {
    super.key,
    this.width,
    this.height,
    this.boxFit = BoxFit.none,
    this.svgColor,
  });

  @override
  Widget build(BuildContext context) {
    if (path.split('.').last.toLowerCase() == 'svg') {
      return SvgPicture.asset(
        path,
        width: width,
        height: height,
        colorFilter: svgColor != null ? ColorFilter.mode(svgColor!, BlendMode.srcIn) : null,
        fit: boxFit,
      );
    } else {
      return Image.asset(path, width: width, height: height, fit: boxFit);
    }
  }
}
