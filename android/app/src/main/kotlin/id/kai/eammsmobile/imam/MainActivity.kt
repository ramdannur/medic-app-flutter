package id.ramdannur.medicapp

import android.content.Context
import android.content.pm.PackageManager
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

class MainActivity : FlutterActivity() {
    private val CHANNEL = "method_channel"

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL).setMethodCallHandler { call, result ->
            if (call.method == "isFlashAvailable") {
                val packageManager: PackageManager = applicationContext.packageManager
                val hasFlash: Boolean = packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
                result.success(hasFlash)
            } else {
                result.notImplemented()
            }
        }
    }
}
